- crear el entorno virtual
    python -m venv venv

- Activar el entorno virtual
    .\venv\Scripts\activate

- Instalar las librerias 
    python -m pip install -r requerimientos.txt
    en fichero de "requerimientos.txt" van las librerias a usar en nuestro proyecto

-Ejecutar el proyecto 
    opcion1: python views.python
    opcion2: python run

