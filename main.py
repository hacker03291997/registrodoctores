from flask import Flask, render_template, request, redirect, url_for
from flask_mysqldb import MySQL

app = Flask(__name__)

# Configuración de la base de datos
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'  # Deberías proporcionar el nombre de usuario si lo hay
app.config['MYSQL_PASSWORD'] = ''  # Deberías proporcionar la contraseña si la hay
app.config['MYSQL_DB'] = 'api_docker_python'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

# Inicializar la extensión MySQL
mysql = MySQL(app)

# Ruta para la pantalla principal
@app.route("/")
def index():
    return render_template("index.html")

# Ruta para el formulario de agregar doctor
@app.route("/agregar_doctor", methods=["GET", "POST"])
def agregar_doctor():
    if request.method == "POST":
        # Obtener datos del formulario
        fullname = request.form['fullname']
        phone = request.form['phone']
        email = request.form['email']
        especialidad = request.form['especialidad']

        # Encontrar el primer ID disponible
        cur = mysql.connection.cursor()
        cur.execute("SELECT id FROM doctores ORDER BY id")
        ids_ocupados = set(row['id'] for row in cur.fetchall())
        cur.close()

        nuevo_id = 1
        while nuevo_id in ids_ocupados:
            nuevo_id += 1

        # Crear cursor y ejecutar la consulta SQL para insertar un nuevo doctor con el ID disponible
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO doctores (id, fullname, phone, email, especialidad) VALUES (%s, %s, %s, %s, %s)",
                    (nuevo_id, fullname, phone, email, especialidad))

        # Confirmar la transacción y cerrar el cursor
        mysql.connection.commit()
        cur.close()

        return redirect(url_for('index'))

    return render_template("agregar_doctor.html")

# Ruta para la página de listar doctores
@app.route("/listar_doctores")
def listar_doctores():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM doctores")
    doctores = cur.fetchall()
    cur.close()
    return render_template("listar_doctores.html", doctores=doctores)

# Ruta para la página de editar doctor
@app.route("/editar_doctor/<int:id>", methods=["GET", "POST"])
def editar_doctor(id):
    cur = mysql.connection.cursor()

    # Obtener la información del doctor a editar
    cur.execute("SELECT * FROM doctores WHERE id = %s", (id,))
    doctor = cur.fetchone()

    if request.method == "POST":
        # Actualizar la información del doctor con los nuevos datos del formulario
        fullname = request.form['fullname']
        phone = request.form['phone']
        email = request.form['email']
        especialidad = request.form.getlist('especialidad')

        # Convertir la lista de especialidades a una cadena separada por comas
        especialidad_str = ",".join(especialidad)

        cur.execute("UPDATE doctores SET fullname=%s, phone=%s, email=%s, especialidad=%s WHERE id=%s",
                    (fullname, phone, email, especialidad_str, id))
        mysql.connection.commit()
        cur.close()

        return redirect(url_for('listar_doctores'))

    cur.close()
    return render_template("editar_doctor.html", doctor=doctor)

# Ruta para eliminar un doctor por ID
@app.route("/eliminar_doctor/<int:id>")
def eliminar_doctor(id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM doctores WHERE id = %s", (id,))
    mysql.connection.commit()
    cur.close()
    return redirect(url_for('listar_doctores'))

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8171)
